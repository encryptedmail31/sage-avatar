<?php

/**
 * @file
 * Contains Drupal\sage_avatar\Service\SGAvatarService.
 */
namespace Drupal\sage_avatar\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * Class SGAvatarService.
 *
 * @package Drupal\sage_avatar
 */
class SGAvatarService {
	/**
   * The HTTP client to fetch the adorable avatar.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  public $httpClient;

  /**
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructor.
   */
  public function __construct(
    ClientInterface $http_client, AccountProxyInterface $current_user) {
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
  }

  /**
   * Headers for TM Call
   * @param $cmd command name
   * @param $acct_id Acount ID
   * @return array of invoice parameters
   */
  public function getEmail() {
    $email = $this->currentUser->getEmail();
    return isset($email) && $email ? $email : '';
  }
}
