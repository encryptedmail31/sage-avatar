<?php

namespace Drupal\sage_avatar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Provides a 'User Avatar Block'.
 *
 * @Block(
 *   id = "user_avatar",
 *   admin_label = @Translation("User Avatar"),
 *   category = @Translation("User Avatar configurations")
 * )
 */
class UserAvatar extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }
  /**
   * {@inheritdoc}
   */
  public function build() {
    $avatar_email = \Drupal::service('sage_avatar.service')->getEmail();
    if(isset($avatar_email) && $avatar_email != '') {
      $avt_url = 'https://api.adorable.io/avatars/200/' . $avatar_email;
    }
    return array(
      '#markup' => '<img src = ' . $avt_url . '>',
      '#cache' => ['max-age' => 0],
    );
  }
}
